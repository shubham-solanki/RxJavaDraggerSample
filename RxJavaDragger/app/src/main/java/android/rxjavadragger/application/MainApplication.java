package android.rxjavadragger.application;

import android.app.Application;
import android.rxjavadragger.dependency.ApiComponent;
import android.rxjavadragger.dependency.DaggerApiComponent;
import android.rxjavadragger.dependency.DaggerNetworkComponent;
import android.rxjavadragger.dependency.NetworkComponent;
import android.rxjavadragger.dependency.NetworkModule;

/**
 * Created by Supriya A on 8/8/2017.
 */

public class MainApplication extends Application {

    public ApiComponent getApiComponent() {
        return mApiComponent;
    }

    private ApiComponent mApiComponent;

    @Override
    public void onCreate() {
        resolveDependency();
        super.onCreate();
    }

    private void resolveDependency() {
        mApiComponent = DaggerApiComponent.builder()
                .networkComponent(getNetworkComponent())
                .build();

    }

    private NetworkComponent getNetworkComponent() {
        return DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule(AppConstants.BASE_URL))
                .build();
    }
}
