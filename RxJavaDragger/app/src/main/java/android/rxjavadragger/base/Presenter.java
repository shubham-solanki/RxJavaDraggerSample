package android.rxjavadragger.base;

/**
 * Created by Supriya A on 8/8/2017.
 */

public interface Presenter {

    void onCreate();

    void onPause();

    void onResume();

    void onDestroy();
}
