package android.rxjavadragger.service;

import android.rxjavadragger.model.FlowerResponse;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Supriya A on 8/8/2017.
 */

public interface FlowerService {

    @GET("/feeds/flowers.json")
    Observable<List<FlowerResponse>> getFlowerList();
}
