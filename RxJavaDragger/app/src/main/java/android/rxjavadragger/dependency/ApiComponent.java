package android.rxjavadragger.dependency;

import android.rxjavadragger.ui.MainActivity;

import dagger.Component;

/**
 * Created by Supriya A on 8/8/2017.
 */

@CustomScope
@Component(modules = ApiModule.class ,dependencies = NetworkComponent.class)
public interface ApiComponent {
    void inject(MainActivity activity);
}
