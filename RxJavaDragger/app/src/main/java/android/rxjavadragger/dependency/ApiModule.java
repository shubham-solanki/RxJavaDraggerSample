package android.rxjavadragger.dependency;

import android.rxjavadragger.service.FlowerService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by Supriya A on 8/8/2017.
 */

@Module
public class ApiModule {

    @Provides
    @CustomScope
    FlowerService provideFlowerServices(Retrofit retrofit){
        return retrofit.create(FlowerService.class);
    }
}
